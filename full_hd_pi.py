import requests
import json
import subprocess
from subprocess import call, check_output

with open('configuration.json') as json_file:
	data = json.load(json_file)

searchUrlPrefix = "https://www.googleapis.com/youtube/v3/search?"
part = data['part']
key = data['key']
order = data['order']
type = data['type']
maxResults = data['maxResults']	
streaming = data['streaming']
blackBackground = data['blackBackground']

newSearch = "y"
resultSet = {}
print("!!!!!YOUTUBE FULLHD PLAYER FOR RASPBERRY PI!!!!!")

while True:
	if newSearch == "y":
		resultSet.clear()
		print("Search by: ", end = '')	
		filter = input()

		finalUrlPrefix = "https://www.youtube.com/watch?v="
		url = searchUrlPrefix + "part=" + part + "&q=" + filter + "&key=" + key + "&maxResults=" + str(maxResults) + "&maxResults=" + str(maxResults) + "&type=" + type + "&order=" + order

		resp = requests.get(url)
		if resp.status_code != 200:
			print("Call failed!!! ")
			print("Response code: " + str(resp.status_code))
			break
		else:   
			result = resp.json()
			for item in result['items']:
				if item["id"] and item["snippet"]:
					for idItem in item["id"]:
						if idItem == "videoId":
							videoId = item["id"]["videoId"]
							videoUrl = finalUrlPrefix + videoId
							videoTitle = item["snippet"]["title"]
							videoDescription = item["snippet"]["description"]
							resultSet.update({videoTitle : videoUrl})
		index = 1	
		print("Result for filter " + filter + ":")			
		for video in resultSet:				
			print(str(index)  + " " + video) # + " " + resultSet[video]
			index = index + 1
			
	if newSearch == "y":
            print("Select video (1,2,3,...,X) or restart select (r): ", end='')
            strChoice = input()
            if strChoice == "r":
                continue
            choice = int(strChoice)
	else:
            choice = int(newSearch)
	index = 1
	selectedVideo = ""
	selectedUrl = ""
	for video in resultSet:	
		if choice == index:
			selectedVideo = video
			selectedUrl = resultSet[video]
			break
		index = index + 1
		
	print("Selected video: " + selectedVideo)
	
	if streaming == True:
            print("Generating stream url:") 
            streamCommand = 'youtube-dl', '-g', '-f', 'best', selectedUrl
            sp = subprocess.check_output(streamCommand)
            url = str(sp)
            urlLen = len(url)
            clearUrl = url[2:urlLen - 3]
            if blackBackground == True:
                playingCommand = 'omxplayer', '--blank', clearUrl    
            else:
                playingCommand = 'omxplayer', clearUrl
            print("Start playing!")
            subprocess.call(playingCommand)
	else:
            fileName = selectedVideo +".mp4"
            mergingCommand = 'youtube-dl', '-f', 'bestvideo[height<=1080][ext=mp4]+bestaudio[ext=m4a]', '--merge-output-format', 'mp4', '-o', selectedVideo, selectedUrl
            if blackBackground == True:
                playingCommand = 'omxplayer', '--blank', fileName    
            else:
                playingCommand = 'omxplayer', fileName
            print("Downloading best quality video and audio. Wait! ")
            subprocess.call(mergingCommand)
            print("Start playing!")
            subprocess.call(playingCommand) 
	print("End of video") 

	print("New search or another video? (y/n/1/2/3/..../X): ", end='') 
	newSearch = input()
	if newSearch == "n":
		resultSet.clear()
		break