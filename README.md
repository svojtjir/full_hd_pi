# full_hd_pi 
Tool for playing HD youtube videos on raspberry pi.

Tired from bad quality and lags of youtube videos in KODI and Chromium?

Good! Try this!

This python script can play youtube videos in full hd quality without lags. 
Sript use HW accelerated player omxplayer. 

Instalation:

1.	Download files or repository

2.	Instal python3

		run in terminal:

		sudo apt update
	
		sudo apt install python3 idle3

3.	Get your own Google api key

		tutorials:

		https://www.youtube.com/watch?v=ysqLvqk3jA4&ab_channel=InEcLabsEmbedded%26AutomotiveLinux (time: 2:24-5:04)

		https://www.youtube.com/watch?v=DcgXSxPkbvY&ab_channel=kuntalin (time: 2:30-4:02)

		https://seo-michael.co.uk/how-to-create-your-own-youtube-api-key-id-and-secret/

		https://www.slickremix.com/docs/get-api-key-for-youtube/

		google!

4.	Add api key to configuration.json. make configuration and save

		maxResults:15	 	//max result items
		
		order:"relevance" 	//posible values: relevance,date,rating,title,videoCount,viewCount
		
		part:"snippet"
		
		type:"video"
		
		key:"**********" 	// here belongs your api key (probably starts with: AIzaSy....)
		
		streaming:false  	// if is streaming set to true then it plays worst quality video, but there is no need to download video before play (faster). for best quality of video set this to false
		
		blackBackground:false	// add black background to video (you can see parts of desktop behind videos with smaller resolution then monitor). but with background on doesnt work video control(pause, volume,...)! 

		default setings is good, so you can add only your api key...

5.	Run terminal in directory with sourcecode
	
		run:
		
		python3 full_hd_pi.py 

6.	Enjoy!